function ll { # dirs first, short to long, files last, long to short and preserve args
    ls -lshBNqp --sort=width    --color=always ${@} | grep --color=never /
    ls -lshBNqp --sort=width -r --color=always ${@} | grep --color=never -v / | sed "1 d"
}


function lll {
    pwd
    echo ${HOME}
    if [ $(pwd) = ${HOME} ]; then
        tree -fiql --dirsfirst --noreport 
    else
        tree -fiql --dirsfirst --noreport --gitignore
    fi
}


function cls {
    clear
    neofetch \
        --ascii_distro Arch_small \
        --kernel_shorthand \
        --disable uptime packages resolution cpu gpu memory icons \
        --color_blocks off 
}


function vich { # Edit scripts fast
    vim $(which $1)
}


function teamviewer {
    killall teamviewer
    systemctl restart teamviewerd
    /usr/bin/teamviewer
}


function mvnrun {
    mvn exec:java -Dexec.mainClass="${@}"
}


function brightness {
    for output in $(xrandr --listactivemonitors | awk '{print $4}')
    do
        xrandr --output ${output} --brightness ${1}
    done
}


function update() {
    curdir=$PWD
    cd /home/e/git/ingar195/arch
    git pull &> /dev/null
    sh install.sh
    cd $curdir

}
