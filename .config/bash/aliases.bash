# Remaps
alias ..='cd ..'        # Save awkward typing
alias df='df -h'
alias rm='rm -d'
alias v='nvim'
alias vi='nvim'         # Avoid using old vi when mistyping
alias vim='nvim'        # Avoid using old vim when mistyping
alias py='bpython'      # Shorter and better
alias mkdir='mkdir -p'  # Why not default?
alias acli='arduino-cli'
alias please="sudo"

# Shortcuts
alias functions='nvim ${XDG_CONFIG_HOME}/bash/functions.bash; source ${XDG_CONFIG_HOME}/bash/bashrc > /dev/null'
alias backups='sudo timeshift --list-snapshots'
alias aliases='nvim ${HOME}/.config/bash/aliases.bash; source ${XDG_CONFIG_HOME}/bash/bashrc > /dev/null'
alias paths='nvim ${HOME}/.config/bash/paths; source ${HOME}/.bashrc > /dev/null'
alias pkgs='nvim -p ${HOME}/.config/pkgs/aur.list ${HOME}/.config/pkgs/arch.list'
alias grps='nvim ${HOME}/.config/groups.list; exec /bin/bash'
alias now='date "+%Y-%m-%d-%H:%M"'
alias rs='rsync --info=progress2 -au'
alias cansetup='sudo ip link set can0 type can bitrate 250000 && sudo ip link set up can0'
alias wifix='sudo systemctl restart iwd; sudo killall dhcpcd; sudo dhcpcd'
alias pvg='ping vg.no'
alias wgu='sudo wg-quick up ${HOME}/.config/wireguard/wg0.conf'
alias wgd='sudo wg-quick down ${HOME}/.config/wireguard/wg0.conf'
alias webcam='ffplay -window_title Webcam -fast /dev/video0'
alias cpu='watch -n.1 "cat /proc/cpuinfo | grep \"^[c]pu MHz\""'
alias ports='ss -tulpn'

# Central git aliases
alias cfg='git --git-dir=${HOME}/.dotfiles --work-tree=${HOME}'
alias plans='git --git-dir=${HOME}/wiki/plans/.git --work-tree=${HOME}/wiki/plans'

# Enabling color
alias ip='ip --color=auto'
alias grep='grep'
alias diff='diff --color=auto'
alias tmux='env TERM=screen-256color tmux'
alias less='less -R --use-color -Dd+m -Du+C -DE+r'


# Cursed
alias no='setxkbmap no'
