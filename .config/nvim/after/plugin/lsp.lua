local lsp = require("lsp-zero")
lsp.on_attach(function(client, bufnr)
	local opts = { buffer = bufnr, remap = false }
	vim.keymap.set("n", "gd", function() vim.lsp.buf.definition() end, opts)
	vim.keymap.set("n", "gD", function() vim.lsp.buf.declaration() end, opts)
	vim.keymap.set("n", "gi", function() vim.lsp.buf.implementation() end, opts)
	vim.keymap.set("n", "go", function() vim.lsp.buf.type_definition() end, opts)
	vim.keymap.set("n", "gr", function() vim.lsp.buf.references() end, opts)
	vim.keymap.set("n", "gs", function() vim.lsp.buf.signature_help() end, opts)
	vim.keymap.set("n", "<F2>", function() vim.lsp.buf.rename() end, opts)
	vim.keymap.set("n", "<F3>", function() vim.lsp.buf.format() end, opts)
	vim.keymap.set("n", "<F4>", function() vim.lsp.buf.code_action() end, opts)
end)

lsp.preset("recommended")
lsp.extend_lspconfig()

-- Mason & Debugging
require('mason').setup({})
require("mason-nvim-dap").setup({
    ensure_installed = {
        "python",
        "javadbg"
    },
    handlers = {}
})

-- LSP Manager
require('mason-lspconfig').setup({
  ensure_installed = {
      "eslint",
      "bashls",
      "clangd",
      "arduino_language_server",
      "ansiblels",
      "cssls",
      "dockerls",
      "docker_compose_language_service",
      "elixirls",
      "html",
      "jsonls",
      "jdtls",
      "tsserver",
      "lua_ls",
      "marksman",
      "nim_langserver",
      "pylsp",
      "lemminx",
      "yamlls"
  },

  handlers = {
    function(server_name)
      require('lspconfig')[server_name].setup({})
    end,},
})


-- Autocomplete
local cmp = require('cmp')
cmp.setup({
  sources = {
    {name = 'nvim_lsp'},
  },
  mapping = {
	["<C-Space>"] = cmp.mapping.complete(),
    ['<C-c>'] = cmp.mapping.abort(),
	["<C-j>"] = cmp.mapping.select_next_item(cmp_select),
	["<C-k>"] = cmp.mapping.select_prev_item(cmp_select),
	["<C-l>"] = cmp.mapping.confirm({ select = true }),
    ['<C-p>'] = cmp.mapping(function()
      if cmp.visible() then
        cmp.select_prev_item({behavior = 'insert'})
      else
        cmp.complete()
      end
    end),
    ['<C-n>'] = cmp.mapping(function()
      if cmp.visible() then
        cmp.select_next_item({behavior = 'insert'})
      else
        cmp.complete()
      end
    end),
  },
  snippet = {
    expand = function(args)
      require('luasnip').lsp_expand(args.body)
    end,
  },
})

lsp.setup()
