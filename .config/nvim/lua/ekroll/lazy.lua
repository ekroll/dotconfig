
-- Ensure lazy is installed
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
    vim.fn.system({
        "git",
        "clone",
        "--filter=blob:none",
        "https://github.com/folke/lazy.nvim.git",
        "--branch=stable", -- latest stable release
        lazypath,
    })
end
vim.opt.rtp:prepend(lazypath)

-- Configure
require("lazy").setup({
	 "mbbill/undotree",
     "tpope/vim-vinegar",

    -- LSP & Debugger Setup
    {"nvim-treesitter/nvim-treesitter",
        build = function()
            pcall(vim.cmd, "TSUpdate")
        end},

	{'VonHeikemen/lsp-zero.nvim',
		branch = 'v3.x',
		dependencies = {
			{'neovim/nvim-lspconfig'},

            -- Plugin Manager
			{'williamboman/mason.nvim',
                build = function()
                    pcall(vim.cmd, 'MasonUpdate')
                end},

            -- LSP and Debugger Mason Plugins
			{'williamboman/mason-lspconfig.nvim'},
            {"jay-babu/mason-nvim-dap.nvim"},

			-- Autocompletion
			{'hrsh7th/nvim-cmp'},     -- Required
			{'hrsh7th/cmp-nvim-lsp'}, -- Required
			{'L3MON4D3/LuaSnip'},     -- Required
		}
	},

    -- Debugger UI
    {'rcarriga/nvim-dap-ui',
        dependencies = {'mfussenegger/nvim-dap',
            dependencies = {"nvim-neotest/nvim-nio"}
        }},

    -- Quick Switcher and Fuzzy Finder
     "theprimeagen/harpoon",
	{'nvim-telescope/telescope.nvim',
        dependencies = {'nvim-lua/plenary.nvim'}},

    -- Git Integration
     "tpope/vim-fugitive",
    {'lewis6991/gitsigns.nvim',
        lazy = true,
        config = function()
            require('gitsigns').setup()
        end },


    -- LSP Setup

    -- Theme & Appearance
    {"lukas-reineke/indent-blankline.nvim",
        lazy = false},

    {'nvim-lualine/lualine.nvim',
        dependencies = {'nvim-tree/nvim-web-devicons'} },

	{'Mofiqul/vscode.nvim',
		as = "vscode",
		config = function()
			vim.o.background = "dark"
			local c = require("vscode.colors").get_colors()
			require("vscode").setup({
				transparent = false,
				italic_comments = true,
				disable_nvimtree_bg = false,
				color_overrides = {
					vscLineNumber = '#777777',
				},

				group_overrides = {
					-- this supports the same val table as vim.api.nvim_set_hl
					--  colors from this colorscheme by requiring vscode.colors!
					Cursor = { fg=c.vscDarkBlue, bg=c.vscLightGreen, bold=true },
                    MatchParen = { bg="#ffaf00", fg="#1c1c1c" },
                    CursorLine = { bg="#555555", cterm=nil }
				}
			})
			require("vscode").load()
		end
	},


    -- Personal Planning
    "vimwiki/vimwiki",
})
