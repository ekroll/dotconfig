# Set user-level variables
export EDITOR='nvim'
export VISUAL='nvim'
export MANPAGER='less'
export AWT_TOOLKIT=MToolkit
export QT_QPA_PLATFORMTHEME=qt5ct
export _JAVA_AWT_WM_NONREPARENTING=1
export HISTCONTROL='erasedups:ignoredups:ignorespace'

export XDG_CONFIG_HOME="${HOME}/.config"
export XDG_DATA_HOME="${HOME}/.local/share"
export XDG_STATE_HOME="${HOME}/.local/state"
export XDG_CACHE_HOME="${XDG_CONFIG_HOME}/cache"


# Source bashrc
bashrc=${XDG_CONFIG_HOME}/bash/bashrc
[ -f "${bashrc}" ] && source ${bashrc}
